<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebqamUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'Webqam',
                'email' => 'register@webqam.fr',
                'email_verified_at' => null,
                'password' => '$2y$10$0ejoa949PhRRiGUr2G9WP.EeumyK7bE2.W/yDuX7nlPoXyabmtWI.',
                'remember_token' => null,
                'created_at' => '2019-06-26 09:05:34',
                'updated_at' => '2019-06-26 09:05:34',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->delete();
    }
}
