<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;

class Task extends \App\Models\Base\Task
{
    use CrudTrait;

	protected $fillable = [
		'title',
		'done',
		'user_id'
	];
}
