<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Jun 2019 11:01:05 +0200.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Task
 * 
 * @property int $id
 * @property string $title
 * @property bool $done
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class Task extends Eloquent
{
	protected $casts = [
		'done' => 'bool',
		'user_id' => 'int'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
